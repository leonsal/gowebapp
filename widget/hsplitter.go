package widget

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
)

type HSplitter struct {
	core.View           // embedded view
	left      *dom.Node // left container node
	right     *dom.Node // right container node
}

const HSplitterClass = "gwen_hsplitter"

// NewHSplitter creates and returns a new horizontal splitter container
func NewHSplitter() *HSplitter {

	s := new(HSplitter)
	s.View.Initialize()
	s.Node = dom.Document().CreateElement("div")
	s.SetClassName(HSplitterClass)

	s.left = dom.Document().CreateElement("span")
	s.AppendChild(s.left)

	s.right = dom.Document().CreateElement("span")
	s.AppendChild(s.right)
	return s
}

func (h *HSplitter) Obj() interface{} {

	return h
}

func (h *HSplitter) SetViewLeft(v dom.INode) {

	h.left.AppendChild(v)
}

func (h *HSplitter) SetViewRight(v dom.INode) {

	h.right.AppendChild(v)
}
