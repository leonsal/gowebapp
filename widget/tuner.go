package widget

import (
	"math"
	"strconv"

	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Tuner struct {
	core.View             // embedded View
	value     int         // tuner current value
	maxValue  int         // maximum value
	minValue  int         // minimum value
	unit      string      // unit name
	unitNode  *dom.Node   // dom node for unit name
	digits    []*dom.Node // list of digits node elements
}

const TunerClass = "gwen_tuner"

// NewTuner creates and returns a new label node with the initial specified text
// template columns and rows
func NewTuner(ndigits int) *Tuner {

	t := new(Tuner)
	t.View.Initialize()
	t.maxValue = math.MaxInt32
	t.unit = " Hz"
	t.digits = make([]*dom.Node, 0)

	// Creates div container and saves its reference
	t.Node = dom.Document().CreateElement("div")
	t.SetClassName(TunerClass)

	for i := 0; i < ndigits; i++ {
		// Creates digit element
		el := dom.Document().CreateElement("span")
		t.AppendChild(el)
		t.digits = append(t.digits, el)
		// Sets tab index to allow focus
		el.SetTabindex(len(t.digits) + 1)

		// Sets event handlers
		index := i
		el.AddEventListener(dom.OnWheel, func(evt dom.Event, ev interface{}) {
			t.onWheel(index, ev.(dom.EventWheel))
		})
		el.AddEventListener(dom.OnKeyDown, func(evt dom.Event, ev interface{}) {
			t.onKeyDown(index, ev.(dom.EventKey))
		})

		// Appends group separator if necessary
		rest := ndigits - 1 - i
		if rest > 0 && (rest%3) == 0 {
			el := dom.Document().CreateElement("span")
			el.SetInnerHTML(".")
			t.AppendChild(el)
		}
	}
	// Adds unit name
	t.unitNode = dom.Document().CreateElement("span")
	t.SetUnit(t.unit)
	t.AppendChild(t.unitNode)

	// Sets initial value
	t.SetValue(t.value, false)
	return t
}

func (t *Tuner) Obj() interface{} {

	return t
}

// SetValue sets the value of this tuner
func (t *Tuner) SetValue(value int, event bool) *Tuner {

	if value < t.minValue || value > t.maxValue {
		return t
	}

	val := value
	for i := 0; i < len(t.digits); i++ {
		pten := math.Pow10(len(t.digits) - i - 1)
		dv := int(math.Floor(float64(val) / pten))
		val = val % int(pten)
		t.digits[i].SetInnerHTML(strconv.Itoa(dv))
	}
	t.value = value

	if event {
		t.Dispatch(core.OnChange, t.value)
	}
	return t
}

// SetValueRange set the range of possible values
func (t *Tuner) SetValueRange(min, max int) *Tuner {

	if min < 0 {
		min = 0
	}
	if min > max {
		panic("Invalid value range")
	}
	t.minValue = min
	t.maxValue = max
	return t
}

// SetUnit set the unit string
func (t *Tuner) SetUnit(unit string) *Tuner {

	t.unitNode.SetInnerHTML(unit)
	return t
}

func (t *Tuner) onWheel(index int, ev dom.EventWheel) {

	if ev.DeltaY < 0 {
		t.decDigit(index)
	} else {
		t.incDigit(index)
	}
}

func (t *Tuner) onKeyDown(index int, ev dom.EventKey) {

	if ev.Key >= "0" && ev.Key <= "9" {
		dv, _ := strconv.Atoi(ev.Key)
		t.setDigit(index, dv)
		t.nextDigit(index)
		return
	}
	switch ev.Key {
	case "ArrowUp":
		t.incDigit(index)
	case "ArrowDown":
		t.decDigit(index)
	case "ArrowRight":
		t.nextDigit(index)
	case "ArrowLeft":
		t.prevDigit(index)
	case "Backspace":
		t.setDigit(index, 0)
		t.prevDigit(index)
	}
}

// Decrements digit value
func (t *Tuner) decDigit(index int) {

	pten := len(t.digits) - index - 1
	value := t.value - int(math.Pow(10, float64(pten)))
	if value < t.minValue {
		return
	}
	t.SetValue(value, true)
}

// Increments digit value
func (t *Tuner) incDigit(index int) {

	pten := len(t.digits) - index - 1
	value := t.value + int(math.Pow(10, float64(pten)))
	t.SetValue(value, true)
}

// Sets key focus to next digit if possible
func (t *Tuner) nextDigit(index int) {

	if index >= len(t.digits)-1 {
		return
	}
	index++
	t.digits[index].Js().Call("focus")
}

// Sets key focus to previous digit if possible
func (t *Tuner) prevDigit(index int) {

	if index <= 0 {
		return
	}
	index--
	t.digits[index].Js().Call("focus")
}

// Sets the value of the specified digit
func (t *Tuner) setDigit(index, vset int) {

	digits := make([]int, 0)
	value := t.value
	for i := 0; i < len(t.digits); i++ {
		pten := math.Pow10(len(t.digits) - i - 1)
		dv := int(math.Floor(float64(value) / pten))
		value = value % int(pten)
		digits = append(digits, dv)
	}

	digits[index] = vset
	value = 0
	for i := 0; i < len(digits); i++ {
		pten := math.Pow10(len(digits) - i - 1)
		value += digits[i] * int(pten)
	}
	t.SetValue(value, true)
}
