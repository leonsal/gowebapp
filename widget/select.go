package widget

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Select struct {
	core.View
}
type SelectOption struct {
	Name  string
	Value string
}

const SelectClass = "gwen_select"
const SelectOptionClass = SelectClass + "_option"

// NewSelect creates and returns a new select node
func NewSelect(options []SelectOption) *Select {

	s := new(Select)
	s.View.Initialize()
	s.Node = dom.Document().CreateElement("select")
	s.SetClassName(SelectClass)
	for _, opt := range options {
		s.AddOption(opt.Name, opt.Value)
	}
	s.AddEventListener(dom.OnChange, func(evt dom.Event, ev interface{}) {
		s.Dispatch(core.OnChange, nil)
	})
	return s
}

func (s *Select) Obj() interface{} {

	return s
}

func (s *Select) AddOption(text, value string) {

	opt := dom.Document().CreateElement("option")
	opt.Set("value", value)
	opt.SetInnerHTML(text)
	opt.SetClassName(SelectOptionClass)
	s.AppendChild(opt)
}

func (s *Select) SetSelected(value string) {

	for i := 0; i < s.ChildCount(); i++ {
		opt := s.ChildAt(i)
		if opt.Js().Get("value").String() == value {
			opt.Js().Set("selected", "true")
			break
		}
	}
}

func (s *Select) SetDisabled(value string, disable bool) {

	for i := 0; i < s.ChildCount(); i++ {
		opt := s.ChildAt(i)
		if opt.Js().Get("value").String() == value {
			if disable {
				opt.Js().Set("disabled", "true")
			} else {
				opt.Js().Set("disabled", "")
			}
			break
		}
	}
}

func (s *Select) DelOption(value string) {

	for i := 0; i < s.ChildCount(); i++ {
		opt := s.ChildAt(i)
		if opt.Js().Get("value").String() == value {
			s.RemoveChild(opt)
			break
		}
	}
}
