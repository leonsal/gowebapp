package widget

import (
	"fmt"
	"syscall/js"

	"gitlab.com/leonsal/gowebapp/css"
	"gitlab.com/leonsal/gowebapp/dom"
	"gitlab.com/leonsal/gowebapp/layout"
)

type Splitter struct {
	*layout.Flex
	sep     *dom.Node
	first   *dom.Node
	second  *dom.Node
	sepDrag bool
	startX  float64
	startY  float64
}

const SplitterClass = "gwen_splitter"

// NewSplitter creates and returns...
func NewSplitter(direction css.Value, first, second dom.INode) *Splitter {

	// Splitter is a Flex layout
	s := new(Splitter)
	s.Flex = layout.NewFlex(css.Flex, direction)

	// Appends the first element
	s.AppendChild(first)
	s.first = first.GetNode()

	// Creates separator div and appends to layout
	s.sep = dom.Document().CreateElement("div")
	sepClass := SplitterClass
	if direction == css.Row {
		sepClass += "_rowsep"
	} else {
		sepClass += "_colsep"
	}
	s.sep.SetClassName(sepClass)

	var cbMouseMove js.Func
	var cbMouseUp js.Func
	s.sep.AddEventListener(dom.OnMouseDown, func(evt dom.Event, ev interface{}) {
		fmt.Printf("Drag ON\n")
		// Saves drag start coordinates
		mev := ev.(dom.EventMouse)
		s.startX = mev.ClientX
		s.startY = mev.ClientY
		// Listen to mouse move events for the Window
		cbMouseMove = dom.Window().AddEventListener(dom.OnMouseMove, func(evt dom.Event, ev interface{}) {
			s.onDrag(ev.(dom.EventMouse))
		})
		// Listen to mouse up events for the Window
		cbMouseUp = dom.Window().AddEventListener(dom.OnMouseUp, func(evt dom.Event, ev interface{}) {
			fmt.Printf("Window Mouse up\n")
			dom.Window().RemoveEventListener(dom.OnMouseMove, cbMouseMove)
			dom.Window().RemoveEventListener(dom.OnMouseUp, cbMouseUp)
			cbMouseMove.Release()
			cbMouseUp.Release()
		})
		s.sepDrag = true
	})
	s.AppendChild(s.sep)

	// Appends the second element
	s.AppendChild(second)
	s.second = second.GetNode()
	return s
}

func (s *Splitter) Obj() interface{} {

	return s
}

func (s *Splitter) onDrag(ev dom.EventMouse) {

	dragX := ev.ClientX
	style := s.first.Get("style")
	width := style.Get("width").String()
	fmt.Printf("onDrag:%v width:%v\n", dragX-s.startX, width)

}
