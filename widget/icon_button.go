package widget

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
	"gitlab.com/leonsal/gowebapp/icon"
)

type IconButton struct {
	*Button            // embedded button
	icon    *icon.Icon // embedded icon
	text    *dom.Node  // span element with text
}

const IconButtonClass = core.CssClass + "_icon_button"

// NewIconButton creates and returns a new icon button node with the initial specified icon code and text
func NewIconButton(icode, text string) *IconButton {

	b := new(IconButton)
	b.Button = NewButton("")
	b.SetClassName(IconButtonClass)

	// Create icon node and appends to button
	b.icon = icon.New(icode)
	b.AppendChild(b.icon)

	// Create span node to contain text and appends to button
	b.text = dom.Document().CreateElement("span")
	b.text.SetClassName(IconButtonClass + "_text")
	b.text.SetInnerHTML(text)
	b.AppendChild(b.text)

	return b
}

func (b *IconButton) Obj() interface{} {

	return b
}

func (b *IconButton) SetIcon(icode string) *IconButton {

	b.icon.SetIcon(icode)
	return b
}

func (b *IconButton) SetText(text string) *IconButton {

	b.text.SetInnerHTML(text)
	return b
}
