package widget

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Button struct {
	core.View
}

const ButtonClass = core.CssClass + "_button"

// NewButton creates and returns a new button node with the initial specified text
// template columns and rows
func NewButton(text string) *Button {

	b := new(Button)
	b.View.Initialize()
	b.Node = dom.Document().CreateElement("button")
	b.Set("type", "button")
	b.SetClassName(ButtonClass)
	b.SetText(text)
	return b
}

func (b *Button) Obj() interface{} {

	return b
}

func (b *Button) SetText(text string) *Button {

	b.SetInnerHTML(text)
	return b
}
