package widget

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Label struct {
	core.View
}

// NewLabel creates and returns a new label node with the initial specified text
// template columns and rows
func NewLabel(text string) *Label {

	l := new(Label)
	l.View.Initialize()

	// Creates div container and saves its reference
	l.Node = dom.Document().CreateElement("div")
	l.SetInnerHTML(text)
	l.SetClassName("gwen_label")
	return l
}

func (l *Label) Obj() interface{} {

	return l
}

func (l *Label) SetText(text string) *Label {

	l.Set("innerHTML", text)
	return l
}
