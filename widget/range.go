package widget

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Range struct {
	core.View
}

const RangeClass = core.CssClass + "_range"

// NewRange creates and returns a new button node with the initial specified text
// template columns and rows
func NewRange() *Range {

	b := new(Range)
	b.View.Initialize()
	b.Node = dom.Document().CreateElement("input")
	b.Set("type", "range")
	b.SetClassName(RangeClass)
	return b
}

func (b *Range) Obj() interface{} {

	return b
}
