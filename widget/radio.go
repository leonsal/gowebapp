package widget

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Radio struct {
	core.View            // embedded label enclosing the <input> and <span>
	input     *core.View // input radio view
	label     *dom.Node  // span element with text
}

const RadioClass = core.CssClass + "_radio"

// NewRadio creates and returns a new radio button node with the initial specified label
func NewRadio(label, group string) *Radio {

	r := new(Radio)
	r.View.Initialize()
	r.Node = dom.Document().CreateElement("label")
	r.SetClassName(RadioClass)

	r.input = core.NewView(dom.Document().CreateElement("input"))
	r.input.Set("type", "radio")
	r.input.Set("name", "group")
	r.AppendChild(r.input)

	r.label = dom.Document().CreateElement("span")
	r.label.SetInnerHTML(label)
	r.AppendChild(r.label)

	return r
}

func (r *Radio) Obj() interface{} {

	return r
}

// Subscribes subscribes for events for the input element of this radio.
func (r *Radio) Subscribe(evt core.Event, cb core.Callback) {

	r.input.Subscribe(evt, cb)
}

// Disable sets the disabled attribute of this radio button
func (r *Radio) SetDisabled(disable bool) *Radio {

	r.input.SetDisabled(disable)
	return r
}
