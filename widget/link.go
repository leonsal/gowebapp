package widget

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Link struct {
	core.View
}

const LinkClass = "gwen_link"

// NewLink creates and returns a new link node with the initial specified text and url
func NewLink(text, url string) *Link {

	l := new(Link)
	l.View.Initialize()
	l.Node = dom.Document().CreateElement("a")
	l.SetClassName(LinkClass)
	l.Set("href", url)
	l.SetInnerHTML(text)
	return l
}

func (l *Link) Obj() interface{} {

	return l
}
