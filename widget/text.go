package widget

import (
	"strconv"

	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Text struct {
	core.View
}

const TextClass = "gwen_input_text"

// NewText creates and returns a new button node with the initial specified text
// template columns and rows
func NewText(text string) *Text {

	t := new(Text)
	t.View.Initialize()
	t.Node = dom.Document().CreateElement("input")
	t.Set("type", "text")
	t.SetClassName(TextClass)
	t.SetValue(text)
	return t
}

func (t *Text) Obj() interface{} {

	return t
}

func (t *Text) SetSize(size int) *Text {

	t.Set("size", strconv.Itoa(size))
	return t
}

func (t *Text) SetValue(text string) *Text {

	t.Set("value", text)
	return t
}

func (t *Text) SetPlaceholder(text string) *Text {

	t.Set("placeholder", text)
	return t
}
