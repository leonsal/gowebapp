package widget

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Checkbox struct {
	core.View            // embedded label enclosing the <input> and <span>
	input     *core.View // input checkbox view
	label     *dom.Node  // text label node
}

const CheckboxClass = core.CssClass + "_checkbox"

// NewCheckbox creates and returns a new radio button node with the initial specified label
func NewCheckbox(label string) *Checkbox {

	r := new(Checkbox)
	r.View.Initialize()
	r.Node = dom.Document().CreateElement("label")
	r.SetClassName(CheckboxClass)

	r.input = core.NewView(dom.Document().CreateElement("input"))
	r.input.Set("type", "checkbox")
	r.AppendChild(r.input)

	r.label = dom.Document().CreateElement("span")
	r.label.SetInnerHTML(label)
	r.AppendChild(r.label)

	return r
}

func (r *Checkbox) Obj() interface{} {

	return r
}

// Subscribes subscribes for events for the input element of this checkbox.
func (r *Checkbox) Subscribe(evt core.Event, cb core.Callback) {

	r.input.Subscribe(evt, cb)
}

// Disable sets the disabled attribute of this checkbox button
func (r *Checkbox) SetDisabled(disable bool) *Checkbox {

	r.input.SetDisabled(disable)
	return r
}
