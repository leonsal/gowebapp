package main

import (
	"sort"

	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
	"gitlab.com/leonsal/gowebapp/widget"
)

type TestMenu struct {
}

func init() {
	testMap["menu"] = &TestMenu{}
}

func (t *TestMenu) Run() {

	keys := make([]string, 0)
	for name := range testMap {
		if name == "menu" {
			continue
		}
		keys = append(keys, name)
	}
	sort.Strings(keys)

	container := dom.Document().CreateElement("div")
	container.SetPadding("10px")
	for _, name := range keys {
		l := widget.NewLink(name, "?t="+name)
		l.SetPadding("0px 0px 10px 10px")
		l.SetDisplay("block")
		container.AppendChild(l)
	}

	core.App().SetView(container)
}
