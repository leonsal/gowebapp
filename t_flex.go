package main

import (
	"fmt"
	"strconv"

	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/css"
	"gitlab.com/leonsal/gowebapp/dom"
	"gitlab.com/leonsal/gowebapp/layout"
	"gitlab.com/leonsal/gowebapp/widget"
)

type TestFlex struct {
}

func init() {
	testMap["Layout_Flex"] = &TestFlex{}
}

func (t *TestFlex) Run() {

	// Creates main container
	container := dom.Document().CreateElement("div")

	// addChild to container
	addChild := func(cont *layout.Flex) {
		child := widget.NewLabel("Child" + strconv.Itoa(cont.ChildCount()+1))
		child.SetBackgroundColor("lightgray")
		cont.AppendChild(child)
	}

	// Creates row flex layout
	f1 := layout.NewFlex(css.Flex, css.Row)
	f1.SetMarginTop("20px")
	f1.SetStyleHeight("100px")
	f1.SetBackgroundColor("yellow")
	// Add children
	for i := 0; i < 5; i++ {
		addChild(f1)
	}

	// Creates column flex layout
	f2 := layout.NewFlex(css.Flex, css.Column)
	f2.SetMarginTop("20px")
	f2.SetStyleHeight("200px")
	f2.SetBackgroundColor("yellow")
	// Add children
	for i := 0; i < 5; i++ {
		addChild(f2)
	}

	// Label for FlexWrap select
	lf := widget.NewLabel("FlexWrap:")
	lf.SetDisplay("inline")
	lf.SetPaddingLeft(css.Px(10))
	container.AppendChild(lf)
	// FlexWrap select
	sf := widget.NewSelect(nil)
	sf.AddOption(string(css.NoWrap), string(css.NoWrap))
	sf.AddOption(string(css.Wrap), string(css.Wrap))
	sf.AddOption(string(css.WrapReverse), string(css.WrapReverse))
	sf.Subscribe(core.OnChange, func(evt core.Event, ev interface{}) {
		fmt.Printf("select:%v\n", sf.Value())
		f1.SetWrap(css.Value(sf.Value()))
		f2.SetWrap(css.Value(sf.Value()))
	})
	container.AppendChild(sf)

	// Label for AlignItems select
	la := widget.NewLabel("AlignItems:")
	la.SetDisplay("inline")
	la.SetPaddingLeft(css.Px(10))
	container.AppendChild(la)
	// AlignItems select
	sa := widget.NewSelect(nil)
	sa.AddOption(string(css.FlexStart), string(css.FlexStart))
	sa.AddOption(string(css.FlexEnd), string(css.FlexEnd))
	sa.AddOption(string(css.Center), string(css.Center))
	sa.AddOption(string(css.Stretch), string(css.Stretch))
	sa.AddOption(string(css.Baseline), string(css.Baseline))
	sa.Subscribe(core.OnChange, func(evt core.Event, ev interface{}) {
		f1.SetAlignItems(css.Value(sa.Value()))
		f2.SetAlignItems(css.Value(sa.Value()))
	})
	container.AppendChild(sa)

	// Label for AlignContent select
	lc := widget.NewLabel("AlignContent:")
	lc.SetDisplay("inline")
	lc.SetPadding("0px 4px")
	container.AppendChild(lc)
	// AlignContent select
	sc := widget.NewSelect(nil)
	sc.AddOption(string(css.FlexStart), string(css.FlexStart))
	sc.AddOption(string(css.FlexEnd), string(css.FlexEnd))
	sc.AddOption(string(css.Center), string(css.Center))
	sc.AddOption(string(css.SpaceBetween), string(css.SpaceBetween))
	sc.AddOption(string(css.SpaceAround), string(css.SpaceAround))
	sc.AddOption(string(css.SpaceEvenly), string(css.SpaceEvenly))
	sc.AddOption(string(css.Stretch), string(css.Stretch))
	sc.Subscribe(core.OnChange, func(evt core.Event, ev interface{}) {
		f1.SetAlignContent(css.Value(sc.Value()))
		f2.SetAlignContent(css.Value(sc.Value()))
	})
	container.AppendChild(sc)

	// Label for JustifyContent select
	lj := widget.NewLabel("JustifyContent:")
	lj.SetDisplay("inline")
	lj.SetPaddingLeft(css.Px(10))
	container.AppendChild(lj)
	// JustifyContent select
	sj := widget.NewSelect(nil)
	sj.AddOption(string(css.FlexStart), string(css.FlexStart))
	sj.AddOption(string(css.Center), string(css.Center))
	sj.AddOption(string(css.FlexEnd), string(css.FlexEnd))
	sj.AddOption(string(css.SpaceAround), string(css.SpaceAround))
	sj.AddOption(string(css.SpaceBetween), string(css.SpaceBetween))
	sj.AddOption(string(css.SpaceEvenly), string(css.SpaceEvenly))
	sj.Subscribe(core.OnChange, func(evt core.Event, ev interface{}) {
		f1.SetJustifyContent(css.Value(sj.Value()))
		f2.SetJustifyContent(css.Value(sj.Value()))
	})
	container.AppendChild(sj)

	// Button to add children
	b1 := widget.NewButton("Add child")
	b1.SetMarginLeft("10px")
	b1.Subscribe(core.OnClick, func(evt core.Event, ev interface{}) {
		addChild(f1)
		addChild(f2)
	})
	container.AppendChild(b1)

	container.AppendChild(f1)
	container.AppendChild(f2)
	core.App().SetView(container)
}
