package main

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/css"
	"gitlab.com/leonsal/gowebapp/dom"
	"gitlab.com/leonsal/gowebapp/widget"
)

type TestSplitter struct {
}

func init() {
	testMap["splitter"] = &TestSplitter{}
}

func (t *TestSplitter) Run() {

	container := dom.Document().CreateElement("div")

	// Left view
	l1 := widget.NewLabel("This is the first label of the panel")
	l1.Get("style").Set("height", "100px")

	// Right view
	l2 := widget.NewLabel("This is the second label of the panel")
	l2.Get("style").Set("height", "100px")

	sp1 := widget.NewSplitter(css.Row, l1, l2)
	sp1.SetBorderTop("1px solid black")
	sp1.SetBorderBottom("1px solid black")

	container.AppendChild(sp1)
	core.App().SetView(container)
}
