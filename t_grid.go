package main

import (
	"fmt"
	"strconv"

	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/css"
	"gitlab.com/leonsal/gowebapp/dom"
	"gitlab.com/leonsal/gowebapp/layout"
	"gitlab.com/leonsal/gowebapp/widget"
)

type TestGrid struct {
}

func init() {
	testMap["Layout_Grid"] = &TestGrid{}
}

func (t *TestGrid) Run() {

	// Creates main container
	container := dom.Document().CreateElement("div")
	var grid *layout.Grid

	// Label for AlignItems select
	la := widget.NewLabel("AlignItems:")
	la.SetDisplay(string(css.Inline))
	la.SetPadding("0px 4px")
	container.AppendChild(la)
	// AlignItems select
	sa := widget.NewSelect(nil)
	sa.AddOption(string(css.Stretch), string(css.Stretch))
	sa.AddOption(string(css.Start), string(css.Start))
	sa.AddOption(string(css.End), string(css.End))
	sa.AddOption(string(css.Center), string(css.Center))
	sa.AddOption(string(css.Baseline), string(css.Baseline))
	sa.AddOption(string(css.FirstBaseline), string(css.FirstBaseline))
	sa.AddOption(string(css.LastBaseline), string(css.LastBaseline))
	sa.Subscribe(core.OnChange, func(evt core.Event, ev interface{}) {
		fmt.Printf("set align items:%v\n", sa.Value())
		grid.SetAlignItems(css.Value(sa.Value()))
	})
	container.AppendChild(sa)

	// Label for JustifyItems select
	lj := widget.NewLabel("JustifyItems:")
	lj.SetDisplay(string(css.Inline))
	lj.SetPadding("0px 4px")
	container.AppendChild(lj)
	// JustifyItems select
	sj := widget.NewSelect(nil)
	sj.AddOption(string(css.Stretch), string(css.Stretch))
	sj.AddOption(string(css.Start), string(css.Start))
	sj.AddOption(string(css.End), string(css.End))
	sj.AddOption(string(css.Center), string(css.Center))
	sj.AddOption(string(css.Left), string(css.Left))
	sj.AddOption(string(css.Right), string(css.Right))
	sj.Subscribe(core.OnChange, func(evt core.Event, ev interface{}) {
		fmt.Printf("set justify items:%v\n", sj.Value())
		grid.SetJustifyItems(css.Value(sj.Value()))
	})
	container.AppendChild(sj)

	// Label for AlignContent select
	lc := widget.NewLabel("AlignContent:")
	lc.SetDisplay(string(css.Inline))
	lc.SetPadding("0px 4px")
	container.AppendChild(lc)
	// AlignContent select
	sc := widget.NewSelect(nil)
	sc.AddOption(string(css.Start), string(css.Start))
	sc.AddOption(string(css.End), string(css.End))
	sc.AddOption(string(css.Center), string(css.Center))
	sc.AddOption(string(css.Stretch), string(css.Stretch))
	sc.AddOption(string(css.SpaceBetween), string(css.SpaceBetween))
	sc.AddOption(string(css.SpaceAround), string(css.SpaceAround))
	sc.AddOption(string(css.SpaceEvenly), string(css.SpaceEvenly))
	sc.Subscribe(core.OnChange, func(evt core.Event, ev interface{}) {
		fmt.Printf("set align content:%v\n", sc.Value())
		grid.SetAlignContent(css.Value(sc.Value()))
	})
	container.AppendChild(sc)

	// Label for JustifyContent select
	ljc := widget.NewLabel("JustifyContent:")
	ljc.SetDisplay(string(css.Inline))
	ljc.SetPadding("0px 4px")
	container.AppendChild(ljc)
	// AlignItems select
	sjc := widget.NewSelect(nil)
	sjc.AddOption(string(css.Start), string(css.Start))
	sjc.AddOption(string(css.End), string(css.End))
	sjc.AddOption(string(css.Center), string(css.Center))
	sjc.AddOption(string(css.Stretch), string(css.Stretch))
	sjc.AddOption(string(css.SpaceBetween), string(css.SpaceBetween))
	sjc.AddOption(string(css.SpaceAround), string(css.SpaceAround))
	sjc.AddOption(string(css.SpaceEvenly), string(css.SpaceEvenly))
	sjc.Subscribe(core.OnChange, func(evt core.Event, ev interface{}) {
		fmt.Printf("set justify content:%v\n", sjc.Value())
		grid.SetJustifyContent(css.Value(sjc.Value()))
	})
	container.AppendChild(sjc)

	// Select child
	sch := widget.NewSelect(nil)
	sch.SetMarginLeft("10px")
	// addChild to grid
	addChild := func(cont *layout.Grid) {
		text := "Child:" + strconv.Itoa(cont.ChildCount()+1)
		child := widget.NewLabel(text)
		child.SetBackgroundColor("lightgray")
		cont.AppendChild(child)
		sch.AddOption(text, text)
	}

	// Button to add children
	b1 := widget.NewButton("Add child")
	b1.SetMarginLeft("10px")
	b1.Subscribe(core.OnClick, func(evt core.Event, ev interface{}) {
		addChild(grid)
	})
	container.AppendChild(b1)
	container.AppendChild(sch)

	// Creates Grid with width less than container width
	// to be able to test AlignContent and JustifyContent
	//grid = layout.NewGrid("repeat(4, 1fr) / repeat(8, 1fr)")
	grid = layout.NewGrid("repeat(4, 50px) / repeat(8, 100px)")
	grid.SetMarginTop("20px")
	grid.SetBackgroundColor("yellow")
	grid.SetGap("4px 4px")
	for i := 0; i < 5; i++ {
		addChild(grid)
	}

	// Input text for grid-area
	it1 := widget.NewText("")
	it1.SetPlaceholder("grid-area")
	it1.SetSize(10)
	it1.SetMarginLeft("10px")
	it1.Subscribe(core.OnKeydown, func(evt core.Event, ev interface{}) {
		kev := ev.(dom.EventKey)
		if kev.Key == "Enter" {
			child := t.findChild(grid, sch.Value())
			if child == nil {
				panic("CHILD NOT FOUND")
			}
			child.SetGridArea(it1.Value())
		}
	})
	container.AppendChild(it1)

	container.AppendChild(grid)
	core.App().SetView(container)
}

func (t *TestGrid) findChild(grid *layout.Grid, text string) *dom.Node {

	for i := 0; i < grid.ChildCount(); i++ {
		child := grid.ChildAt(i).GetNode()
		if child.InnerHTML() == text {
			return child
		}
	}
	return nil
}
