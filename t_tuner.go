package main

import (
	"fmt"

	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
	"gitlab.com/leonsal/gowebapp/widget"
)

type TestTuner struct {
}

func init() {
	testMap["tuner"] = &TestTuner{}
}

func (t *TestTuner) Run() {

	container := dom.Document().CreateElement("div")

	t1 := widget.NewTuner(7)
	t1.SetValue(50000, false)
	t1.SetValueRange(100, 2000000)
	t1.Subscribe(core.OnChange, func(evt core.Event, ev interface{}) {
		fmt.Printf("Tuner1 changed:%v\n", ev)
	})
	container.AppendChild(t1)

	t2 := widget.NewTuner(6)
	t2.SetValue(0, false)
	t2.SetValueRange(1, 800000)
	t2.SetUnit(" mhz")
	t2.Subscribe(core.OnChange, func(evt core.Event, ev interface{}) {
		fmt.Printf("Tuner2 changed:%v\n", ev)
	})
	container.AppendChild(t2)

	core.App().SetView(container)
}
