package css

import (
	"fmt"
	"strconv"
	"strings"
)

func Px(values ...int) string {

	res := make([]string, len(values))
	for i, v := range values {
		res[i] = strconv.Itoa(v) + "px"
	}
	fmt.Printf("Px:%v\n", strings.Join(res, " "))
	return strings.Join(res, " ")
}
