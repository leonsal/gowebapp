package css

type Value string

const (
	Auto          Value = "auto"
	Baseline      Value = "baseline"
	Center        Value = "center"
	Column        Value = "column"
	ColumnReverse Value = "column-reverse"
	End           Value = "end"
	FirstBaseline Value = "first baseline"
	FlexStart     Value = "flex-start"
	FlexEnd       Value = "flex-end"
	Flex          Value = "flex"
	Stretch       Value = "stretch"
	Inline        Value = "inline"
	InlineFlex    Value = "inline-flex"
	LastBaseline  Value = "last baseline"
	Left          Value = "left"
	Normal        Value = "normal"
	NoWrap        Value = "nowrap"
	Right         Value = "right"
	Row           Value = "row"
	RowReverse    Value = "row-reverse"
	SpaceAround   Value = "space-around"
	SpaceBetween  Value = "space-between"
	SpaceEvenly   Value = "space-evenly"
	Start         Value = "start"
	Wrap          Value = "wrap"
	WrapReverse   Value = "wrap-reverse"
)
