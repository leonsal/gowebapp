package main

import (
	"net/url"

	"gitlab.com/leonsal/gowebapp/dom"
)

// Interface for all tests object
type ITest interface {
	Run()
}

// maps the test name string to its object
// Individual tests sets the keys of this map
var testMap = map[string]ITest{}

func main() {

	// Get and parse the suplied browser url
	href := dom.Window().Get("location").Get("href").String()
	url, err := url.Parse(href)
	if err != nil {
		panic(err)
	}

	// Get the query values ("?t=<testName>")
	values := url.Query()
	value, ok := values["t"]
	var testName string
	if ok {
		testName = value[0]
	} else {
		testName = "menu"
	}

	// Checks if the test name is valid and runs test
	itest, ok := testMap[testName]
	if !ok {
		panic("INVALID TEST NAME")
	}

	//core.Manager().Clear()
	itest.Run()

	// To keep program running
	ch := make(chan bool)
	<-ch
}
