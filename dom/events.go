package dom

import (
	"syscall/js"
)

type Event string

const (
	OnClick     Event = "click"     // Widget clicked by mouse left button or key
	OnMouseDown Event = "mousedown" // any mouse button is pressed
	OnMouseUp   Event = "mouseup"   // any mouse button is released
	OnMouseMove Event = "mousemove" // mouse position events
	OnKeyDown   Event = "keydown"   // key is pressed
	OnKeyUp     Event = "keyup"     // key is released
	OnKeyPress  Event = "keypress"  // key is pressed and released
	OnResize    Event = "resize"    // panel size changed (no parameters)
	OnLoad      Event = "load"      // resource loaded
	OnWheel     Event = "wheel"     // mouse wheel event
	OnChange    Event = "change"    // select option changed
)

type EventMouse struct {
	Type     Event   // event type
	ClientX  float64 // x coordinate of viewport position
	ClientY  float64 // y coordinate of viewport position
	ScreenX  float64 // x coordinate of screen position
	ScreenY  float64 // y coordinate of screen position
	AltKey   bool    // true if alt modifier was active
	CtrlKey  bool    // true if Control modifier was active
	ShiftKey bool    // true if Shift modifier was active
	MetaKey  bool    // true if Meta modifier was active
	Button   int
	Buttons  int
}

type EventKey struct {
	Type     Event  // event type
	Key      string // unicode string or special Key Value
	Code     string // identifies the physical key being pressed
	Repeat   bool   // true if keys has been pressed long enough to trigger key repetion
	AltKey   bool   // true if alt modifier was active
	CtrlKey  bool   // true if Control modifier was active
	ShiftKey bool   // true if Shift modifier was active
	MetaKey  bool   // true if Meta modifier was active
}

type EventWheel struct {
	Type      Event   // event type
	DeltaX    float64 // horizontal scroll amount
	DeltaY    float64 // vertical scroll amount
	DeltaZ    float64 // scroll amount for z-axis
	DeltaMode int     // unit of delta values
}

type EventResize struct {
	Type Event // event type
}

type EventLoad struct {
	Type Event // event type
}

func buildEvent(evjs js.Value) (Event, interface{}) {

	namejs := evjs.Get("type").String()
	switch namejs {
	// Mouse events
	case "click", "mousedown", "mouseup", "mousemove":
		var ev EventMouse
		ev.ClientX = evjs.Get("clientX").Float()
		ev.ClientY = evjs.Get("clientY").Float()
		ev.ScreenX = evjs.Get("screenX").Float()
		ev.ScreenY = evjs.Get("screenY").Float()
		ev.AltKey = evjs.Get("altKey").Bool()
		ev.CtrlKey = evjs.Get("ctrlKey").Bool()
		ev.ShiftKey = evjs.Get("shiftKey").Bool()
		ev.MetaKey = evjs.Get("metaKey").Bool()
		ev.Button = evjs.Get("button").Int()
		ev.Buttons = evjs.Get("buttons").Int()
		switch namejs {
		case "click":
			ev.Type = OnClick
		case "mousedown":
			ev.Type = OnMouseDown
		case "mouseup":
			ev.Type = OnMouseUp
		case "mousemove":
			ev.Type = OnMouseMove
		}
		return ev.Type, ev
	// Key events
	case "keydown", "keypress", "keyup":
		var ev EventKey
		ev.Key = evjs.Get("key").String()
		ev.Code = evjs.Get("code").String()
		ev.AltKey = evjs.Get("altKey").Bool()
		ev.CtrlKey = evjs.Get("ctrlKey").Bool()
		ev.ShiftKey = evjs.Get("shiftKey").Bool()
		ev.MetaKey = evjs.Get("metaKey").Bool()
		switch namejs {
		case "keydown":
			ev.Type = OnKeyDown
		case "keypress":
			ev.Type = OnKeyPress
		case "keyup":
			ev.Type = OnKeyUp
		}
		return ev.Type, ev
	case "resize":
		var ev EventResize
		ev.Type = OnResize
		return ev.Type, ev
	case "load":
		var ev EventLoad
		ev.Type = OnLoad
		return ev.Type, ev
	case "wheel":
		var ev EventWheel
		ev.DeltaX = evjs.Get("deltaX").Float()
		ev.DeltaY = evjs.Get("deltaY").Float()
		ev.DeltaZ = evjs.Get("deltaZ").Float()
		ev.DeltaMode = evjs.Get("deltaMode").Int()
		return ev.Type, ev
	default:
		return "UNKNOWN:", nil
	}
	return "", nil
}
