package dom

import (
	"syscall/js"
)

type WindowObj struct {
	Node
}

// Window singleton instance
var _windowInstance *WindowObj

// Window returns the instance of the Window singleton object
func Window() *WindowObj {

	if _windowInstance != nil {
		return _windowInstance
	}

	_windowInstance := new(WindowObj)
	_windowInstance.SetJs(js.Global().Get("window"))
	return _windowInstance
}
