package dom

import (
	"strconv"
	"syscall/js"
)

// INode is the interface for all view types
type INode interface {
	Js() js.Value // Js returns this view Javascript dom reference
	GetNode() *Node
	Obj() interface{} // Obj returns the pointer to the view object
}

type Node struct {
	jsv js.Value // javascript object reference
}

// Js returns this node Javascript object reference
func (n *Node) Js() js.Value {

	return n.jsv
}

// SetJs sets this node Javascript object reference
func (n *Node) SetJs(el js.Value) {

	n.jsv = el
}

// GetNode satisfies the INode interface
func (n *Node) GetNode() *Node {

	return n
}

// Obj satisfies the INode interface
func (n *Node) Obj() interface{} {

	return nil
}

// Get get the value of the specified property of this node's Javascript value
func (n *Node) Get(p string) js.Value {

	return n.jsv.Get(p)
}

// Set sets the specified property of this node's Javascript value
func (n *Node) Set(p string, x interface{}) {

	n.jsv.Set(p, x)
}

func (n *Node) AddEventListener(evtype Event, cb func(Event, interface{})) js.Func {

	jscb := js.FuncOf(func(this js.Value, args []js.Value) interface{} {
		evtype, ev := buildEvent(args[0])
		cb(evtype, ev)
		return nil
	})
	n.jsv.Call("addEventListener", string(evtype), jscb)
	return jscb
}

func (n *Node) RemoveEventListener(evtype Event, cb js.Func) {

	n.jsv.Call("removeEventListener", string(evtype), cb)
}

// AppendChild appends a child to this node.
// Both the DOM and internal list are updated
func (n *Node) AppendChild(child INode) {

	n.jsv.Call("appendChild", child.Js())
}

// RemoveChild removes the specified child view from this view and from the DOM.
// Returns the node found or nil otherwise
func (n *Node) RemoveChild(child INode) INode {

	found := n.jsv.Call("removeChild", child.Js())
	if found == js.Undefined() {
		return nil
	}
	return &Node{found}
}

func (n *Node) ChildElementCount() int {

	return n.jsv.Get("childElementCount").Int()
}

func (n *Node) FirstChild() *Node {

	return &Node{n.jsv.Get("firstChild")}
}

func (n *Node) SetClassName(name string) {

	n.Set("className", name)
}

func (n *Node) SetInnerHTML(text string) {

	n.Set("innerHTML", text)
}

func (n *Node) SetDisabled(disable bool) {

	n.Set("disabled", disable)
}

func (n *Node) SetTabindex(idx int) {

	if idx < 1 {
		panic("invalid tab index")
	}
	n.jsv.Set("tabIndex", strconv.Itoa(idx))
}

func (n *Node) Style() js.Value {

	return n.Get("style")
}

func (n *Node) SetStyleHeight(height string) {

	s := n.Get("style")
	s.Set("height", height)
}

func (n *Node) SetBackgroundColor(color string) {

	s := n.Get("style")
	s.Set("background-color", color)
}

func (n *Node) SetMargin(margin string) {

	s := n.Get("style")
	s.Set("margin", margin)
}

func (n *Node) SetMarginTop(top string) {

	s := n.Get("style")
	s.Set("marginTop", top)
}

func (n *Node) SetMarginRight(right string) {

	s := n.Get("style")
	s.Set("marginRight", right)
}

func (n *Node) SetMarginBottom(bottom string) {

	s := n.Get("style")
	s.Set("marginBottom", bottom)
}

func (n *Node) SetMarginLeft(left string) {

	s := n.Get("style")
	s.Set("marginLeft", left)
}

func (n *Node) SetBorder(border string) {

	s := n.Get("style")
	s.Set("border", border)
}

func (n *Node) SetBorderTop(border string) {

	s := n.Get("style")
	s.Set("borderTop", border)
}

func (n *Node) SetBorderRight(border string) {

	s := n.Get("style")
	s.Set("borderRight", border)
}

func (n *Node) SetBorderLeft(border string) {

	s := n.Get("style")
	s.Set("borderLeft", border)
}

func (n *Node) SetBorderBottom(border string) {

	s := n.Get("style")
	s.Set("borderBottom", border)
}

func (n *Node) SetBorderWidth(border string) {

	s := n.Get("style")
	s.Set("borderWidth", border)
}

func (n *Node) SetPadding(padding string) {

	s := n.Get("style")
	s.Set("padding", padding)
}

func (n *Node) SetPaddingLeft(padding string) {

	s := n.Get("style")
	s.Set("paddingLeft", padding)
}

func (n *Node) SetDisplay(display string) {

	s := n.Get("style")
	s.Set("display", display)
}

func (n *Node) Value() string {

	return n.Js().Get("value").String()
}

func (n *Node) InnerHTML() string {

	return n.Js().Get("innerHTML").String()
}

func (n *Node) SetValue(val string) {

	n.Js().Set("value", val)
}

func (n *Node) SetGridArea(area string) {

	s := n.Get("style")
	s.Set("gridArea", area)
}

func (n *Node) SetGridColumnStart(start string) {

	s := n.Get("style")
	s.Set("gridColumnStart", start)
}

func (n *Node) SetGridColumnEnd(end string) {

	s := n.Get("style")
	s.Set("gridColumnEnd", end)
}

func (n *Node) SetGridColumn(col string) {

	s := n.Get("style")
	s.Set("gridColumn", col)
}

func (n *Node) SetGridRow(row string) {

	s := n.Get("style")
	s.Set("gridRow", row)
}

func (n *Node) SetGridRowStart(start string) {

	s := n.Get("style")
	s.Set("gridRowStart", start)
}

func (n *Node) SetGridRowEnd(end string) {

	s := n.Get("style")
	s.Set("gridRowEnd", end)
}

func (n *Node) SetAlignSelf(align string) {

	s := n.Get("style")
	s.Set("alignSelf", align)
}

func (n *Node) SetJustifySelf(justify string) {

	s := n.Get("style")
	s.Set("justifySelf", justify)
}
