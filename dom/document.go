package dom

import (
	"fmt"
	"syscall/js"
)

type DocumentObj struct {
	jsv js.Value
}

// Document singleton instance
var docInstance *DocumentObj

// Document returns the instance of the Document singleton object
func Document() *DocumentObj {

	if docInstance != nil {
		return docInstance
	}
	docInstance = &DocumentObj{js.Global().Get("document")}
	return docInstance
}

func (d *DocumentObj) CreateElement(el string) *Node {

	return &Node{d.jsv.Call("createElement", el)}
}

func (d *DocumentObj) CreateTextNode(text string) *Node {

	return &Node{d.jsv.Call("createTextNode", text)}
}

func (d *DocumentObj) GetElementsByTagName(tag string) []*Node {

	list := d.jsv.Call("getElementsByTagName", tag)
	length := list.Get("length").Int()
	nodes := make([]*Node, length)
	for i := 0; i < length; i++ {
		nodes[i] = &Node{list.Index(i)}
	}
	fmt.Printf("nodes:%v\n", nodes)
	return nodes
}
