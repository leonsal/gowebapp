package dom

func Px(v ...int) string {

	return ""
}

func Pt(v ...int) string {

	return ""
}

type AlignItems string

const (
	AlignItemsStart AlignItems = "start"
)

type Value string

const (
	Center       = "center"
	FlexStart    = "flex-start"
	FlexEnd      = "flex-end"
	SpaceBetween = "space-between"
	SpaceAround  = "space-around"
	Stretch      = "stretch"
)
