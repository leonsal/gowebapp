package core

type Event string

const (
	OnClick     Event = "OnClick"   // view clicked
	OnChange    Event = "OnChange"  // view value changed
	OnKeydown   Event = "OnKeydown" // view value changed
	OnMousedown Event = "OnMousedown"
)
