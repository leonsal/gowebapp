package core

import (
	"gitlab.com/leonsal/gowebapp/dom"
)

type View struct {
	*dom.Node              // embedded node
	Dispatcher             // embedded event dispatcher
	children   []dom.INode // optional children
}

// NewView creates and returns a pointer to a View from the specified dom Node object.
func NewView(n *dom.Node) *View {

	v := new(View)
	v.Node = n
	v.Dispatcher.Initialize()
	return v
}

func (v *View) Initialize() {

	v.Dispatcher.Initialize()
}

func (v *View) Subscribe(evt Event, cb Callback) {

	switch evt {
	case OnChange:
		v.AddEventListener(dom.OnChange, func(evt dom.Event, ev interface{}) {
			v.Dispatch(OnChange, nil)
		})
		v.Dispatcher.Subscribe(evt, cb)
	case OnClick:
		v.AddEventListener(dom.OnClick, func(evt dom.Event, ev interface{}) {
			v.Dispatch(OnClick, ev.(dom.EventMouse))
		})
		v.Dispatcher.Subscribe(evt, cb)
	case OnKeydown:
		v.AddEventListener(dom.OnKeyDown, func(evt dom.Event, ev interface{}) {
			v.Dispatch(OnKeydown, ev.(dom.EventKey))
		})
		v.Dispatcher.Subscribe(evt, cb)
	default:
		v.Dispatcher.Subscribe(evt, cb)
	}
}

func (v *View) AppendChild(child dom.INode) {

	v.Node.AppendChild(child)
	v.children = append(v.children, child)
}

// RemoveChild removes the specified child view from this view and from the DOM.
// Returns the view found or nil otherwise
func (v *View) RemoveChild(child dom.INode) dom.INode {

	// Remove from the DOM first
	v.Node.RemoveChild(child)

	// Try to remove from internal list
	var found dom.INode
	for pos, current := range v.children {
		if current == child {
			found = current
			copy(v.children[pos:], v.children[pos+1:])
			v.children[len(v.children)-1] = nil
			v.children = v.children[:len(v.children)-1]
			return found
		}
	}
	return nil
}

func (v *View) ChildCount() int {

	return len(v.children)
}

func (v *View) ChildAt(index int) dom.INode {

	return v.children[index]
}
