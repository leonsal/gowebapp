package core

import (
	"gitlab.com/leonsal/gowebapp/dom"
)

type AppObj struct {
	view dom.INode
}

const CssClass = "gwen"

var appInstance *AppObj

func App() *AppObj {

	if appInstance != nil {
		return appInstance
	}

	a := new(AppObj)
	appInstance = a
	return appInstance
}

// SetView sets the application main view replacing any previous view
func (a *AppObj) SetView(view dom.INode) {

	// Remove all child elements from body (body[0])
	body := dom.Document().GetElementsByTagName("body")[0]
	for {
		count := body.ChildElementCount()
		if count == 0 {
			break
		}
		n := body.FirstChild()
		body.RemoveChild(n)
	}

	// Sets the only child
	body.AppendChild(view)
	a.view = view
}

// View returns the application current main application view
func (a *AppObj) View() dom.INode {

	return a.view
}
