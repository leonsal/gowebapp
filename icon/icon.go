package icon

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Icon struct {
	core.View
}

const IconClass = core.CssClass + "_icon"

// New creates and returns a new icon node with the specified icon codepoint
func New(icode string) *Icon {

	i := new(Icon)
	i.View.Initialize()
	i.Node = dom.Document().CreateElement("i")
	i.SetClassName("material-icons")
	i.Get("classList").Call("add", IconClass)
	i.SetInnerHTML(icode)
	return i
}

func (i *Icon) Obj() interface{} {

	return i
}

func (i *Icon) SetIcon(icode string) {

	i.SetInnerHTML(icode)
}
