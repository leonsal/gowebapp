package layout

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/css"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Grid struct {
	core.View
}

const GridClass = "gwen_grid"

// NewGrid creates and returns a new grid layout view with the initial specified grid-template
func NewGrid(template string) *Grid {

	g := new(Grid)

	// Creates div container and saves its reference
	g.Node = dom.Document().CreateElement("div")
	g.SetClassName(GridClass)

	// Sets initial styles
	style := g.Js().Get("style")
	style.Set("display", "grid")
	g.SetTemplate(template)
	style.Set("height", "300px")
	//style.Set("width", "100vw")
	//style.Set("height", "100vh")

	return g
}

// Obj satisfies the INode interface
func (g *Grid) Obj() interface{} {

	return g
}

func (g *Grid) SetGap(gap string) {

	style := g.Js().Get("style")
	style.Set("gridGap", gap)
}

func (g *Grid) SetRowGap(rowgap string) {

	style := g.Js().Get("style")
	style.Set("gridRowGap", rowgap)
}

func (g *Grid) SetColumnGap(colgap string) {

	style := g.Js().Get("style")
	style.Set("gridColumnGap", colgap)
}

func (g *Grid) SetTemplate(template string) {

	style := g.Js().Get("style")
	style.Set("gridTemplate", template)
}

func (g *Grid) SetTemplateColumns(template string) {

	style := g.Js().Get("style")
	style.Set("gridTemplateColumns", template)
}

func (g *Grid) SetTemplateRows(template string) {

	style := g.Js().Get("style")
	style.Set("gridTemplateRows", template)
}

// JustifyContent sets the "justify-content" property of this grid style
// to justify the complete grid along the row axis when the grid is smaller
// than its container. The suported values are:
// css.Start, css.End, css.Center, css.Stretch, css.SpaceAround,
// css.SpaceBetween andcss.SpaceEvenly.
func (g *Grid) SetJustifyContent(justify css.Value) {

	style := g.Js().Get("style")
	style.Set("justifyContent", string(justify))
}

func (g *Grid) SetJustifyItems(justify css.Value) {

	style := g.Js().Get("style")
	style.Set("justifyItems", string(justify))
}

// AlignContent sets the "align-content" property of this grid style
// to align the complete grid along the column axis when the grid is smaller
// than its container. The suported values are:
// css.Start, css.End, css.Center, css.Stretch, css.SpaceAround,
// css.SpaceBetween andcss.SpaceEvenly.
func (g *Grid) SetAlignContent(align css.Value) {

	style := g.Js().Get("style")
	style.Set("alignContent", string(align))
}

func (g *Grid) SetAlignItems(align css.Value) {

	style := g.Js().Get("style")
	style.Set("alignItems", string(align))
}

func (g *Grid) SetGridAutoFlow(flow css.Value) {

	style := g.Js().Get("style")
	style.Set("gridAutoFlow", flow)
}
