package layout

import (
	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/css"
	"gitlab.com/leonsal/gowebapp/dom"
)

type Flex struct {
	core.View
}

const FlexClass = "gwen_flex"

// NewFlex creates and returns a new flex layout view with specified type and direction
func NewFlex(flexType css.Value, direction css.Value) *Flex {

	// Creates div container and saves its reference
	f := new(Flex)
	f.Node = dom.Document().CreateElement("div")
	f.SetClassName(FlexClass)

	// Sets initial styles
	style := f.Js().Get("style")
	style.Set("display", string(flexType))
	style.Set("flexDirection", string(direction))

	return f
}

// Obj satisfies the INode interface
func (f *Flex) Obj() interface{} {

	return f
}

func (f *Flex) SetWrap(wrap css.Value) {

	style := f.Js().Get("style")
	style.Set("flexWrap", string(wrap))
}

func (f *Flex) SetAlignItems(align css.Value) {

	style := f.Js().Get("style")
	style.Set("alignItems", string(align))
}

func (f *Flex) SetJustifyContent(justify css.Value) {

	style := f.Js().Get("style")
	style.Set("justifyContent", string(justify))
}

func (f *Flex) SetAlignContent(align css.Value) {

	style := f.Js().Get("style")
	style.Set("alignContent", string(align))
}
