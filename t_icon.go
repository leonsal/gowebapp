package main

import (
	"sort"

	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/css"
	"gitlab.com/leonsal/gowebapp/dom"
	"gitlab.com/leonsal/gowebapp/icon"
	"gitlab.com/leonsal/gowebapp/layout"
	"gitlab.com/leonsal/gowebapp/widget"
)

type TestIcon struct {
}

func init() {
	testMap["icon"] = &TestIcon{}
}

func (t *TestIcon) Run() {

	// Creates Grid container
	grid := layout.NewGrid("repeat(4, 1fr) / repeat(5, 1fr)")
	grid.SetGap("0px 0px")
	grid.SetAlignItems(css.Stretch)
	grid.SetJustifyItems(css.Stretch)

	// Get all icon names and sort them
	names := make([]string, 0)
	for name, _ := range icon.Name2Codepoint {
		names = append(names, name)
	}
	sort.Strings(names)

	// Add child divs with icon name and icon image
	for _, name := range names {
		div := dom.Document().CreateElement("div")
		div.SetBorderLeft("1px solid lightgray")
		div.SetBorderBottom("1px solid lightgray")
		div.SetPadding("2px")
		div.AppendChild(widget.NewLabel(name))
		el := icon.New(icon.Name2Codepoint[name])
		div.AppendChild(el)
		grid.AppendChild(div)
	}

	core.App().SetView(grid)
}
