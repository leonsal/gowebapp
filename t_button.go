package main

import (
	"fmt"
	"strconv"

	"gitlab.com/leonsal/gowebapp/core"
	"gitlab.com/leonsal/gowebapp/css"
	"gitlab.com/leonsal/gowebapp/icon"
	"gitlab.com/leonsal/gowebapp/layout"
	"gitlab.com/leonsal/gowebapp/widget"
)

type TestButton struct {
}

func init() {
	testMap["button"] = &TestButton{}
}

func (t *TestButton) Run() {

	// Creates Grid container
	grid := layout.NewGrid("repeat(4, 1fr) / repeat(5, 1fr)")
	grid.SetGap("0px 0px")
	grid.SetAlignItems(css.Center)
	grid.SetJustifyItems(css.Center)

	tabIndex := 1
	// Normal buttons
	count := 1
	for i := 0; i < 5; i++ {
		text := "Button:" + strconv.Itoa(count)
		count++
		b := widget.NewButton(text)
		b.SetTabindex(tabIndex)
		b.Subscribe(core.OnClick, func(evt core.Event, ev interface{}) {
			fmt.Printf("OnClick: %v\n", text)
		})
		if i%2 != 0 {
			b.SetDisabled(true)
		}
		grid.AppendChild(b)
		tabIndex++
	}

	// Icon buttons
	icons := []string{icon.Alarm, icon.AlarmAdd, icon.AlarmOff, icon.AlarmOn, icon.Album}
	for i := 0; i < 5; i++ {
		text := "Button:" + strconv.Itoa(count)
		count++
		b := widget.NewIconButton(icons[i], text)
		b.SetTabindex(tabIndex)
		b.Subscribe(core.OnClick, func(evt core.Event, ev interface{}) {
			fmt.Printf("OnClick: %v\n", text)
		})
		if i%2 != 0 {
			b.SetDisabled(true)
		}
		grid.AppendChild(b)
		tabIndex++
	}

	// Radio buttons
	for i := 0; i < 5; i++ {
		label := "Radio:" + strconv.Itoa(i+1)
		group := "group"
		r := widget.NewRadio(label, group)
		r.SetTabindex(tabIndex)
		r.Subscribe(core.OnClick, func(evt core.Event, ev interface{}) {
			fmt.Printf("OnClick: %v\n", label)
		})
		grid.AppendChild(r)
		if i%2 != 0 {
			r.SetDisabled(true)
		}
		tabIndex++
	}

	// Checkbox
	for i := 0; i < 5; i++ {
		label := "Check:" + strconv.Itoa(i+1)
		r := widget.NewCheckbox(label)
		r.SetTabindex(tabIndex)
		r.Subscribe(core.OnClick, func(evt core.Event, ev interface{}) {
			fmt.Printf("OnClick: %v\n", label)
		})
		grid.AppendChild(r)
		if i%2 != 0 {
			r.SetDisabled(true)
		}
		tabIndex++
	}

	// Range
	for i := 0; i < 5; i++ {
		label := "Range:" + strconv.Itoa(i+1)
		r := widget.NewRange()
		r.SetTabindex(tabIndex)
		r.Subscribe(core.OnChange, func(evt core.Event, ev interface{}) {
			fmt.Printf("OnChange: %v\n", label)
		})
		grid.AppendChild(r)
		if i%2 != 0 {
			r.SetDisabled(true)
		}
		tabIndex++
	}

	core.App().SetView(grid)
}
